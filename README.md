# GitLab Extension

Published to the [Chrome Web Store](https://chrome.google.com/webstore/detail/gitlab-details-sidebar/pmgjbjfdmgljhnnhhoccjgpabhgefnki) and [Mozilla Add-ons](https://addons.mozilla.org/en-US/firefox/addon/gitlab-details-sidebar/).

## Custom domains

For using the GitLab Details Sidebar on a custom domain for GitLab, once you have installed the extension from one of the links above:

* Navigate to your custom GitLab instance
* Right click on the extension icon
* Select "Enable GitLab Details Sidebar on this domain"
* Accept the permissions dialog
* Click "Ok" to reload the page

![Screenshot of the "Enable GitLab Details Sidebar on this domain" option in the right click menu](docs/img/custom-domain.png)

## Getting up and running

```
nvm use
npm i
npm start
```

## Building / deploying

### Using GitLab CI

To release a new version, make sure you've bumped version in manifest.json since previous release, then push to master (through a MR) and when the build job is finished
click the "Play" button on the release job - this will (hopefully) release a new version

### Manually

```
npm run build
```

This will generate the css file needed. You'll then need to bundle everything up into a zip folder for upload to the chrome web store

## Sidebar enhancement

Idea is based off [designs provided by GitLab](https://gitlab.com/gitlab-org/gitlab-foss/issues/24656) for a potential future enhancement.
[Google Chrome Extension Documentation](https://developer.chrome.com/extensions/overview)
