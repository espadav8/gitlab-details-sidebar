import 'webext-dynamic-content-scripts';
import addDomainPermissionToggle from 'webext-domain-permission-toggle';

addDomainPermissionToggle();

const onHeadersReceived = (details) => {
  const { responseHeaders } = details;
  const X_FRAME_OPTIONS_HEADER = 'x-frame-options';
  const X_FRAME_OPTIONS_VALUE = 'SAMEORIGIN';

  if (details.frameId === 0 || details.parentFrame === -1) {
    return { responseHeaders };
  }

  const updatedResponseHeader = responseHeaders.map((header) => {
    const { name } = header;
    if (name.toLowerCase() === X_FRAME_OPTIONS_HEADER) {
      return {
        name,
        value: X_FRAME_OPTIONS_VALUE,
      };
    }
    return header;
  });

  return { responseHeaders: updatedResponseHeader };
};

chrome.webRequest.onHeadersReceived.addListener(
  onHeadersReceived,
  {
    urls: ['*://*/*/-/issues/*']
  },
  [
    'blocking',
    'responseHeaders',
  ]
);
