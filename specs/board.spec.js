const puppeteer = require('puppeteer');
const { expect } = require('chai');

const TIMEOUT = 120000;
const opts = {
  args: [
    '--disable-dev-shm-usage', // Needed for CI
    '--disable-extensions-except=./dist',   
    '--disable-gbu',
    '--load-extension=./dist',
  ],
  headless: false,
  slowMo: 100,
  timeout: TIMEOUT,
};

describe('GitLab Details Sidebar extension', function() {
  this.timeout(TIMEOUT);
  let browser, page;

  before(async function() {
    browser = await puppeteer.launch(opts);
  });

  after(function() {
    browser.close();
  });

  beforeEach(async function() {
    page = await browser.newPage();
    page.setViewport({
      height: 768,
      width: 1024,
    });
    await page.goto('https://gitlab.com/potato-oss/gitlab-extensions/ci-test-project/-/boards');
    await page.waitForSelector('.board li.board-card', { visible: true });
  });

  afterEach(async function() {
    // eslint-disable-next-line no-undef
    await page.evaluate(() => window.localStorage.clear());
    await page.close();
  })

  it('injects items into the sidebar', async function() {
    expect(await page.$('.sidebar-iframe')).to.be.null;
    expect(await page.$('.gitlab-sidebar-expanded-toggle')).to.be.null;

    await page.click('.board li.board-card');

    expect(await page.$('.sidebar-iframe')).not.to.be.null;
    expect(await page.$('.gitlab-sidebar-expanded-toggle')).not.to.be.null;
  });

  it('only injects one iframe', async function() {
    await page.click('.board li.board-card');

    await page.waitForSelector('.sidebar-iframe', { visible: true });
    
    const sidebarIframes = await page.$$('.sidebar-iframe');
    expect(sidebarIframes.length).to.equal(1);
  });

  it('opens the sidebar by default', async function() {
    await page.click('.board li.board-card');

    const bodyHasClass = await page
      .$eval('body', body => body.classList.contains('gitlab-sidebar-detail--collapsed'));
    expect(bodyHasClass).to.be.false;

    const sidebarIframe = await page.$('.sidebar-iframe');
    expect(await sidebarIframe.isIntersectingViewport()).to.be.true;
  });

  it('closes the sidebar when icon is pressed', async function() {
    await page.click('.board li.board-card');

    await page.click('.gitlab-sidebar-expanded-toggle');

    const bodyHasClass = await page
      .$eval('body', body => body.classList.contains('gitlab-sidebar-detail--collapsed'));
    expect(bodyHasClass).to.be.true;

    const sidebarIframe = await page.$('.sidebar-iframe');
    expect(await sidebarIframe.isIntersectingViewport()).to.be.false;
  });

  it('remembers the closed state', async function() {
    await page.click('.board li.board-card');

    await page.click('.gitlab-sidebar-expanded-toggle');

    await page.click('.board li.board-card:last-child');

    const sidebarIframe = await page.$('.sidebar-iframe');
    expect(await sidebarIframe.isIntersectingViewport()).to.be.false;
  });

  const viewportWidths = [
    1200,
    1400,
    2000
  ];

  viewportWidths.forEach((viewportWidth) => {
    it(`should allow all boards to be reachable via scroll at ${viewportWidth}px`, async function() {
      page.setViewport({
        height: 800,
        width: viewportWidth,
      });

      await page.click('.board li.board-card');

      await page.waitForSelector('.sidebar-iframe', { visible: true });

      await page.$eval('.boards-list', boardlist => boardlist.scrollBy(boardlist.scrollWidth, 0));

      const lastColumn = await page.$('.board:last-child');
      const lastColumnSizing = await lastColumn.boundingBox();
      const sidebarIframe = await page.$('.sidebar-iframe');
      const sidebarIframeSizing = await sidebarIframe.boundingBox();

      expect(lastColumnSizing.x + lastColumnSizing.width).to.be.lessThan(sidebarIframeSizing.x);
    });
  });
});
