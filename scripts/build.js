const AdmZip = require('adm-zip');
const fs = require('fs');
const path = require('path');

const zip = new AdmZip();

const INPUT = path.join(__dirname, '..', 'dist');
const OUTPUT = 'bundle.zip';

async function zipExtension(dir) {
  const files = await fs.promises.readdir(dir);

  files
    .forEach((file) => {
      const filePath = path.join(dir, file);
      zip.addLocalFile(filePath);
      console.debug(`${file} added to ${OUTPUT}`);
    })

  const outputPath = path.join(dir, OUTPUT);
  console.log(`Writing ${outputPath}`)
  zip.writeZip(outputPath);
}

zipExtension(INPUT)
  .catch(e => console.error(e));
